<?php

namespace App\Lib\Slack\Commands;

use App\Lib\Slack\SlackBotCommand;

/**
 * Represents "invoice" slack bot command.
 * @package App\Lib\Slack\Commands
 */
class InvoiceCommand implements SlackBotCommand
{
    /**
     * Try to parse command from string.
     * @param $raw string Raw string.
     * @return array Array of parsed arguments.
     */
    public function parse($raw)
    {
        $matches = [];

        if (!preg_match('/invoice\samount\:\s(?P<amount>[0-9\.]+)\scomment\:\s(?P<comment>[\W\w]+)/i', $raw, $matches))
            return null;

        return ['amount' => doubleval($matches['amount']), 'comment' => $matches['comment']];
    }
}