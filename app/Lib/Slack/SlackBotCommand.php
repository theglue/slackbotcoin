<?php

namespace App\Lib\Slack;

/**
 * Represents a single Slack bot command.
 * @package App\Lib
 */
interface SlackBotCommand
{
    /**
     * Try to parse command from string.
     * @param $raw string Raw string.
     * @return array Array of parsed arguments.
     */
    public function parse($raw);
}