<?php
/**
 * Created by PhpStorm.
 * User: gogzor
 * Date: 12/01/16
 * Time: 18:41
 */

namespace App\Lib\Slack\Commands;


use App\Lib\Slack\SlackBotCommand;

/**
 * Represents "rates" slack bot command.
 * @package App\Lib\Slack\Commands
 */
class RatesCommand implements SlackBotCommand
{
    /**
     * Try to parse command from string.
     * @param $raw string Raw string.
     * @return array Array of parsed arguments.
     */
    public function parse($raw)
    {
        $matches = [];

        if (!preg_match('/rates\b/i', $raw, $matches))
            return null;

        return [];
    }
}