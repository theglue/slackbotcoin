<?php

namespace App\Http\Controllers;

use App\Lib\Slack\SlackBot;
use Illuminate\Http\Request;

/**
 * Represents a Slack "/slash command" responder.
 * @package App\Http\Controllers
 */
class BotController extends Controller
{
    /**
     * Responds to requested command.
     * @return string
     */
    public function respond(Request $request)
    {
        // Extract neccessary request data.
        $command = $request->input('text');
        $token = $request->input('token');

        // Check Slack token.
        if ($token !== env('SLACK_TOKEN', null))
            return 'Invalid Slack token provided!';

        // Instantiate SlackBot.
        $slackBot = new SlackBot();

        // Try parse command.
        $parsedCommand = $slackBot->parseCommand($command);

        // If given command is not available.
        // Return bad message.
        if ($parsedCommand === null)
            return "Given command \"$command\" is not valid!";

        // Process command and return it's response.
        return $slackBot->executeFromCommand($parsedCommand, $request->input());
    }
}