<?php

namespace App\Lib;

use App\User;
use Unirest;

/**
 * Provides a convenient facade class to work with Cryptopay API.
 * Documentation: https://github.com/cryptopay-dev/cryptopay-api
 *
 * @package App\Lib
 */
class Cryptopay
{

    /**
     * Contains current instance's API key.
     */
    private $_apiKey;

    private $_cryptopayBaseDomain = 'https://cryptopay.me/api';
    private $_cryptopayApiVersion = 'v1';

    /**
     * Creates a new invoice with given BTC amount and comment.
     * @param float $amount
     * @param string $comment
     * @return string
     */
    public function createInvoice($amount, $comment)
    {
        $response = Unirest\Request::post(
            "$this->_cryptopayBaseDomain/$this->_cryptopayApiVersion/invoices",
            ['Content-Type' => 'application/json'],
            json_encode([
                'api_key' => $this->_apiKey,
                'currency' => 'BTC',
                'price' => $amount,
                'description' => $comment
            ]));

        // Extract response data.
        $btc_price = $response->body->btc_price;
        $btc_address = $response->body->btc_address;
        $invoice_url = $response->body->url;
        $currency = $response->body->currency;
        $bitcoin_uri = $response->body->bitcoin_uri;

        // Build response string.
        $result = "Invoice created. Awaiting $btc_price $currency on $btc_address\n";
        $result .= "Cryptopay invoice URL: $invoice_url\nBitcoin URI: $bitcoin_uri";

        return $result;
    }

    /**
     * Returns current BTC rates from Cryptopay.
     */
    public function getCurrentRates()
    {
        $response = Unirest\Request::get("$this->_cryptopayBaseDomain/$this->_cryptopayApiVersion/rates");
        $eur = $response->body->EUR;
        $gbp = $response->body->GBP;
        $usd = $response->body->USD;

        return "Current BTC rates are: $eur EUR / $gbp GBP / $usd USD";
    }

    /**
     * Connects Slack user with given Cryptopay API key.
     * @param $slack_user_id string Slack user id.
     * @param $slack_user_name string Slack user name.
     * @param $crypto_api_key string Cryptopay API key.
     * @return string
     */
    public function connectAccount($slack_user_id, $slack_user_name, $crypto_api_key)
    {
        // Manually validate cryptopay api key format.
        if (!preg_match('/([0-9a-z]{32})/i', $crypto_api_key))
            return "Provided Cryptopay API key is in invalid format!";

        // Get user from db.
        User::firstOrCreate([
            'slack_id' => $slack_user_id,
            'cryptopay_key' => $crypto_api_key
        ]);

        return "Hey $slack_user_name! We have successfully linked your Cryptopay API key \"$crypto_api_key\".";
    }

    /**
     * Api key setter.
     * @param $apiKey
     * @return mixed
     */
    public function setApiKey($apiKey)
    {
        $this->_apiKey = $apiKey;
    }
}