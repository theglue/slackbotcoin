<?php

namespace App\Lib\Slack\Commands;

use App\Lib\Slack\SlackBotCommand;

/**
 * Represents "connect" slack bot command.
 * @package App\Lib\Slack\Commands
 */
class ConnectCommand implements SlackBotCommand
{
    /**
     * Try to parse command from string.
     * @param $raw string Raw string.
     * @return array Array of parsed arguments.
     */
    public function parse($raw)
    {
        $matches = [];

        if (!preg_match('/connect\skey\:\s(?P<api_key>[a-z0-9]+)/i', $raw, $matches))
            return null;

        return ['api_key' => $matches['api_key']];
    }
}