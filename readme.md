### SlackBotCoin - an easy way to manage your Cryptopay BitCoin account using Slack messages!

#### Usage

First you have to connect your **Cryptopay API Key** to your current **Slack** user.
To do so, just enter command `/botcoin connect key: 98de0b233379a17ad90fce7e9bc191b8` (fake API key here).

Notice that all commands follow syntax `/botcoin command_name argument: value`.

#### List of available commands

* **connect** - Connect your current Slack user to provided Cryptopay API Key.
    Example: `/botcoin connect key: 98de0b233379a17ad90fce7e9bc191b8`
    
* **rates** - Return current BTC rates in EUR, GBP, USD currencies.
    Example: `/botcoin rates`
    
* **invoice** - Create new invoice to receive BTC.
    Example: `/botcoin invoice amount: 0.0452 comment: For candies!`
    
    
#### Installation

**SlackBotCoin** built on **Laravel 5** web framework, an easiest way to launch application is by using laravel's [Homestead](https://laravel.com/docs/5.2/homestead) **VAGRANT** environment.
All you have to do is provide settings (such as db connection) in **.env** (use **env-example** for reference) file.
After that you should just run migrations `php artisan migrate` to create database schema and you are ready to go.

**P.S** I named a command **/botcoin**, but you can use any name when registering your **slash command** in Slack.

#### Local machine usage

To test route from local machine, just send POST requests to `/botcoin`.
For example, to create invoice, send data:

    POST /botcoin
    
    user_id=U10000000
    user_name=Test
    token=slack_token_should_match_with_env_file_setting
    text=invoice amount: 0.0512 comment: Cool!


#### App structure

The application has only one single route `POST /botcoin` and **App\Http\Controllers\BotController** that handles requests.

Other classes:

- **App\Lib\Cryptopay** - provides functionality to work with Cryptopay API.
- **App\Lib\Slack\SlackBot** - provides functionality to process bot commands.
- **App\Lib\Slack\SlackBotCommand** - interface for bot commands, every command should implement it.
- **App\Lib\Slack\Commands** - all bot commands handlers reside in this namespace.