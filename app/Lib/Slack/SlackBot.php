<?php

namespace App\Lib\Slack;

use App\Lib\Cryptopay;
use App\Lib\Slack\Commands\ConnectCommand;
use App\Lib\Slack\Commands\InvoiceCommand;
use App\Lib\Slack\Commands\RatesCommand;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Represents a Slack bot.
 * @package App\Lib
 */
class SlackBot
{
    /**
     * Current Cryptopay class instance.
     * @type Cryptopay
     */
    private $_cryptoPayInstance;

    /**
     * List of available /botcoin commands and their parsers.
     */
    private $_availableCommands;

    /**
     * Checks whether specified user has API set already or not.
     * @param $slack_user_id string Slack user id.
     * @return bool
     */
    private function _apiKeyAlreadySet($slack_user_id)
    {
        try {
            $user = User::where('slack_id', $slack_user_id)->first();
            if ($user === null || $user->cryptopay_key === null)
                return false;
        } catch (ModelNotFoundException $ex) {
            return false;
        }
        return true;
    }

    /**
     * SlackBot constructor.
     */
    public function __construct()
    {
        // Initialize available commands.
        $this->_availableCommands = [
            'connect' => new ConnectCommand(),
            'invoice' => new InvoiceCommand(),
            'rates' => new RatesCommand()
        ];
    }

    /**
     * Convert raw command text to usable command and it's arguments.
     * @param $rawCommand string Raw Slack text representation of a command.
     * @return array Returns array with command name and arguments.
     */
    public function parseCommand($rawCommand)
    {
        $result = null;

        // Iterate over all available commands.
        foreach ($this->_availableCommands as $name => $command) {

            // Try to parse current command's arguments.
            $arguments = $command->parse($rawCommand);

            // Skip to next command, if current failed to match.
            if ($arguments === null) continue;

            // If succeed, return parsed.
            return ['name' => $name, 'args' => $arguments];
        }

        return $result;
    }

    /**
     * Execute operation for requested command.
     * @param array $commandData
     * @param array $requestData
     * @return string
     */
    public function executeFromCommand($commandData, $requestData = [])
    {
        $commandName = $commandData['name'];
        $commandArgs = $commandData['args'];

        // Precheck API key for all commands except "connect".
        if ($commandName !== "connect" && !$this->_apiKeyAlreadySet($requestData['user_id']))
            return "You have to link your Cryptopay API key first. Use \"connect\" command.";

        // Instantiate Cryptopay class.
        $this->_cryptoPayInstance = new Cryptopay();

        // Process "connect" command.
        if ($commandName === "connect")
            return $this->_cryptoPayInstance->connectAccount(
                $requestData['user_id'],
                $requestData['user_name'],
                $commandArgs['api_key']);

        // For all other commands assumed that key exists, assign it.
        $this->_cryptoPayInstance->setApiKey(
            User::where('slack_id', $requestData['user_id'])->first()->cryptopay_key);

        // Process all commands except "connect" here.
        try {
            switch ($commandName) {
                case 'rates':
                    return $this->_cryptoPayInstance->getCurrentRates();
                case 'invoice':
                    return $this->_cryptoPayInstance->createInvoice($commandArgs['amount'], $commandArgs['comment']);
            }
        } catch (\Exception $ex) {
            return 'An error occured while processing your request. Please try again in a minute...';
        }
    }
}